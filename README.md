# Frontend Code Challenge

The point of this exercise is to get a better understanding of the way you think and code. We'd like to see how you go about your solution, how you structure the code, what you consider important and what not, etc.

**Please do not feel like you have to finish everything in the allotted time.**

## The Challenge

Create a currency converter calculator, same as google's, that takes three (to four) inputs, calculates the converted sum, and displays it in an en_DE locale.

[Here's an example that converts 100 Euros to dollars.](https://bit.ly/2KQMbo5) The example might not show the result in the requested locale.

![Google Example:](./calculator-example.png)

Google's converter also has a graph that shows the conversion over time. This feature is **not** required to be implemented.

### Assets
* The currency codes and the currency names can be consumed directly from the consts file: `src/consts/CurrencyCodes.js` or from the redux state in global.currencyCodes. Each currency should have only one occurrence in the currency selector inputs.
* The currency conversion rate should be fetched from the free api: https://free.currencyconverterapi.com/
* This project was created with create-react-app. If you need any additional information please read `Create-React-App-README.me` or lookup `create react app`.


### General instructions
* The current project has the following dependencies already added:
  * react
  * react-dom
  * redux
  * recompose
  * reselect
  * Jest

  Please feel free to add any additional library you think you will need, but please have a good argument why you need it. Also do not feel forced to use any of the existing libraries (save for React).
* You can use any JS edition which is supported by babel. Feel free to change the configuration if you wish to use features which are not supported out of the box in create-react-app.
* To get started:
  * Clone the project (or download).
  * Make sure you have Node >= 6 on your local development machine.
  * run `yarn` to install dependencies or `npm install` if you don't have yarn.
  * Run`yarn start` or `npm start` to run the development server.
  * Run 'yarn test' or 'npm test' to run tests.