import React, { Component } from "react";
import { Provider } from "react-redux"
import { createStore } from "redux"
import rootReducer from "./reducers"
import HomeContainer from "./pages/Home/HomeContainer";
import styles from "./App.module.css";

const store = createStore(rootReducer,
  window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
);

class App extends Component {
  render() {
    return (
      <Provider store={store}>
        <div className={styles.applicationWrapper}>
          <HomeContainer />
        </div>
      </Provider>
    );
  }
}

export default App;
