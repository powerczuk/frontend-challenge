import React from "react";
import PropTypes from "prop-types";
import cardStyles from "./Card.module.css";

const Card = ({ children, className }) => {
  const cardClassName = [cardStyles.card, className].join(" ");

  return (
    <div className={cardClassName}>
      {children}
    </div>
  );
};

Card.defaultProps = {
  className: undefined
};

Card.propTypes = {
  children: PropTypes.node.isRequired,
  className: PropTypes.string
};

export default Card;
