import React from "react";
import Card from "../Card/Card";
import { DropdownInput, TextInput } from "../Inputs";
import currencies from "../../consts/CurrencyCodes";
import styles from "./CurrencyExchanger.module.css";

const getCurrenciesOptions = () => {
  const uniqueCurrencyNumericCodes = [...new Set(currencies.map(currency => currency.NumericCode))];

  return uniqueCurrencyNumericCodes.map((numericCode) => {
    const foundCurrency = currencies.find(item => item.NumericCode === numericCode);
    return {
      label: foundCurrency.Currency,
      value: numericCode
    };
  });
};

const CurrencyExchanger = () => (
  <Card className={styles.currencyExchanger}>
    <div className={styles.currencyExchangerHeader}>
      <span className={styles.currencyExchangerPrimaryTitle}>100 Euro</span>
      <span className={styles.currencyExchangerSecondaryTitle}>100 Euro</span>
    </div>
    <div className={styles.currencyExchangerBody}>
      <div className={styles.currencyExchangerInputs}>
        <div>
          <TextInput id="primaryCurrencyAmount" />
          <DropdownInput id="primaryCurrency" options={getCurrenciesOptions()} />
        </div>
        <div>
          <TextInput id="secondaryCurrencyAmount" />
          <DropdownInput id="secondaryCurrency" options={getCurrenciesOptions()} />
        </div>
      </div>
      <div className={styles.currencyExchangerChart}>Chart</div>
    </div>
  </Card>
);

export default CurrencyExchanger;
