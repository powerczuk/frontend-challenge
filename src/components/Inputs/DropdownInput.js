import React, { Component } from "react";
import PropTypes from "prop-types";
import styles from "./DropdownInput.module.css";

class DropdownInput extends Component {
  constructor(props) {
    super(props);

    this.state = {
      value: props.selectedValue
    };
  }

  handleChangeOption = ({ target }) => {
    const { onChange } = this.props;
    const { value } = target;
    this.setState({ value });

    return onChange(value);
  };

  renderSelectOptions = options => options.map((option) => {
    const { value, label } = option;

    return <option key={value} value={value}>{label}</option>;
  });

  render() {
    const { name, options } = this.props;
    const { value } = this.state;

    return (
      <select className={styles.dropdownInput} name={name} onChange={this.handleChangeOption} value={value}>
        {options && this.renderSelectOptions(options)}
      </select>
    );
  }
}

DropdownInput.defaultProps = {
  name: "DropdownComponent",
  onChange: () => "",
  options: [
    {
      label: "No options provided",
      value: 0
    }
  ],
  selectedValue: 0
};

DropdownInput.propTypes = {
  name: PropTypes.string,
  onChange: PropTypes.func,
  options: PropTypes.arrayOf(
    PropTypes.shape({
      label: PropTypes.string,
      value: PropTypes.number
    })
  ),
  selectedValue: PropTypes.number
};

export default DropdownInput;
