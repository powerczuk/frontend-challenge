import React, { Component } from "react";
import PropTypes from "prop-types";
import styles from "./TextInput.module.css";

class TextInput extends Component {
  constructor(props) {
    super(props);
    this.state = {
      value: props.value
    };
  }

  handleChangeValue = ({ target }) => {
    const { onChange } = this.props;
    const { value } = target;
    this.setState({ value });

    return onChange(value);
  };

  render() {
    const { name, placeholder } = this.props;
    const { value } = this.state;

    return (
      <input
        className={styles.textInput}
        name={name}
        onChange={this.handleChangeValue}
        placeholder={placeholder}
        value={value}
        type="text"
      />
    );
  }
}

TextInput.defaultProps = {
  name: "TextInputComponent",
  onChange: () => "",
  placeholder: "",
  value: ""
};

TextInput.propTypes = {
  name: PropTypes.string,
  onChange: PropTypes.func,
  placeholder: PropTypes.string,
  value: PropTypes.string
};

export default TextInput;
