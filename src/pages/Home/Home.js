import React from "react";
import CurrencyExchanger from "../../components/CurrencyExchanger/CurrencyExchanger";

const Home = () => <CurrencyExchanger />;

export default Home;
