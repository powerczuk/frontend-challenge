import currencyCodes from "../consts/CurrencyCodes";

const defaultState = {
  currencyCodes
};
export default function (state = defaultState, action) {
  switch (action.type) {
    default:
      return state;
  }
}
